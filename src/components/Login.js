import './cssComponentes/Login.css';
export default function LoginUser() {
    return(
        <div background="./imgComponentes/login.jpg">
            <form action="index.html" method="get" className="form-login" > 
                <h5>Login</h5>
                <input className="control"  id="usuario" type="text" name="usuario"  placeholder="Usuario" required />
                <input className="control" id ="contrasena"  type="password" name="contrasena" placeholder="Contraseña" required />
                <input type="button" id="btningresar" className="boton" />
                <p><a href="olvidocontra.html">¿olvido su contraseña?</a></p>
            </form>
        </div>
    );
} 
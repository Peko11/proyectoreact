import './cssComponentes/HeaderFooter.css'
import Cabecera from "./Header";
import BarraIzquierda from "./BarraIzquierda";
import { Routes, Route } from "react-router-dom";
import Administrar_usuario from "./Administrar-usuario";
export default function PagePrincipal() {
  return (
    <>
      <div className="content-header">
        <Cabecera />
      </div>
      <div className="bodyIndex">
        <BarraIzquierda />
        <div className="derecha">
          <Routes>
            <Route path="/" element={<BarraIzquierda />} />
            <Route path="/Administrar-usuario" element={<Administrar_usuario />} />
          </Routes>
        </div>
      </div>
    </>
  );
}

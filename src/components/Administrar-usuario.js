import { Button, Modal } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import "./cssComponentes/Registro.css";
import { useState } from "react";
import ModalRegistro from "./ModalRegistro";
import ModalEliminar from "./ModalEliminar";
export default function Administrar_usuario() {
  const [open, setopen] = useState(false);
  const [open2, setopen2] = useState(false);
  const [open3, setopen3] = useState(false);
  return(
    <>
      <section id="container">
        <h1>Control de usuarios</h1>
        <Button onClick={() => setopen(!open)} color="primary"> Crear usuario</Button>
        <table id="resultado">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Usuario</th>
              <th>Rol</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody id="resultadoBody">
            <tr id="U1">
              <td>U1</td>
              <td>Romina</td>
              <td>info@romina.com</td>
              <td>roma</td>
              <td>Admin</td>
              <td>
                <Button  onClick={() => setopen2(!open2)} color="btn btn-success">Editar</Button>
              </td>
            </tr>
            <tr id="U2">
              <td>U1</td>
              <td>Romina</td>
              <td>info@romina.com</td>
              <td>roma</td>
              <td>Admin</td>
              <td>
                <Button  onClick={() => setopen2(!open2)} color="btn btn-success">Editar</Button> .
                <Button  onClick={() => setopen3(!open3)} color="btn btn-danger">Eliminar</Button>
              </td>
            </tr>
          </tbody>
        </table>
      </section>
      <ModalRegistro estado={open} setestado={setopen} Titulo="Registrar Usuario"/>
      <ModalRegistro estado={open2} setestado={setopen2} Titulo="Editar Usuario"/>
      <ModalEliminar estado={open3} setestado={setopen3}/>

      
    </>
  );
}

import './cssComponentes/Registro.css'
import {Button,Modal,ModalBody,ModalFooter,ModalHeader} from "reactstrap";

export default function ModalEliminar(props) {
  return (
    <>
      <Modal isOpen={props.estado}>
        <ModalHeader>
          ¿Está seguro de eliminar el siguiente registro?
        </ModalHeader>
        <ModalFooter>
          <Button color='btn btn-danger'>Eliminar</Button>
          <Button onClick={()=> props.setestado(false)} color='btn btn-success'> Cerrar </Button>
        </ModalFooter>
      </Modal>
    </>
  );
}

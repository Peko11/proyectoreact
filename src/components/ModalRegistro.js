import {Button,Modal,ModalBody,ModalFooter,ModalHeader} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
export default function ModalRegistro(props) {
   

  return (
    <>
      <Modal isOpen={props.estado}>
        <ModalHeader>{props.Titulo}</ModalHeader>
        <ModalBody>
          <label for="nombre">Nombre</label>
          <input required name="nombre" type="text" id="nombre" placeholder="Nombre completo"/>
          
          <label for="correo">Correo electrónico</label>
          <input required name="correo" type="email" id="correo" placeholder="Correo electrónico"/>
          
          <label for="usuario">Usuario</label>
          <input required name="usuario" type="text" id="usuario" placeholder="Usuario"/>
          
          <label for="clave">Clave</label>
          <input required name="clave" type="password" id="clave" placeholder="Clave de acceso"/>
        </ModalBody>
        <ModalFooter>
          <Button color="Primary">Registrar</Button>
          <Button onClick={() => props.setestado(false)} color="Secondary"> Cerrar </Button>
         
        </ModalFooter>
      </Modal>
    </>
  );
}

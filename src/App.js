import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginUser from "./components/Login";
import PagePrincipal from "./components/PagePricipal";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/Login" element={<LoginUser />}></Route>
        <Route path="/SistemaGD/*" element={<PagePrincipal />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
export default App;
